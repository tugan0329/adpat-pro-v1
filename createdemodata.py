import json
import random
from datetime import date


class DemoData:

    demo_accounts = []
    demo_clients = []
    demo_firms = []
    demo_projects = []
    demo_documents = []

    def create_demo_data(self):
        self.demo_clients = self.create_demo_clients()
        self.demo_firms = self.create_demo_firms()
        self.demo_accounts = self.create_demo_accounts()
        self.demo_projects = self.create_demo_projects()
        self.demo_documents = self.create_demo_documents()

    def create_demo_accounts(self):
        demo_accounts = []
        curr_id = 1
        for demo_client in self.demo_clients:
            demo_account = {'id': curr_id, 'email': 'account{}@{}.com'.format(curr_id, demo_client['name']),
                            'password': 'password', 'first_name': "Account", 'last_name': '{}'.format(curr_id),
                            'client_id': demo_client['id'], 'firm_id': None}
            demo_accounts.append(demo_account)
            curr_id += 1
        for demo_firm in self.demo_accounts:
            for i in range(2):
                demo_account = {'id': curr_id, 'email': 'account{}@{}.com'.format(i, demo_firm['name']),
                                'password': 'password', 'first_name': "Account", 'last_name': '{}'.format(curr_id),
                                'client_id': None, 'firm_id': demo_firm['id']}
                demo_accounts.append(demo_account)
                curr_id += 1
        self.write_json(demo_accounts, './data/accounts.txt')
        return demo_accounts

    def create_demo_clients(self):
        demo_clients = []
        for i in range(3):
            demo_client = {'id': i, 'name': 'Client{}'.format(i), 'email': 'contact@client{}.com'.format(i),
                           'phone': '{}{}{} {}{}{} {}{}{}{}'.format(i, i, i, i, i, i, i, i, i, i),
                           'address': '42 Wallabee Way Sydney, Australia'}
            demo_clients.append(demo_client)
        self.write_json(demo_clients, './data/clients.txt')
        return demo_clients

    def create_demo_firms(self):
        demo_firms = []
        for i in range(3):
            demo_firm = {'id': i, 'name': 'Firm{}'.format(i), 'email': 'contact@firm{}.com'.format(i),
                         'phone': '{}{}{} {}{}{} {}{}{}{}'.format(i, i, i, i, i, i, i, i, i, i),
                         'address': '42 Wallabee Way Sydney, Australia'}
            demo_firms.append(demo_firm)
        self.write_json(demo_firms, './data/firms.txt')
        return demo_firms

    def create_demo_projects(self):
        demo_projects = []
        curr_id = 1
        for i in range(3):
            demo_project = {'id': curr_id, 'name': '{} Tax Filing'.format(self.demo_clients[i]['name']),
                            'start_date': str(date.today()), 'end_date': None, 'client_id': self.demo_clients[i]['id'],
                            'firm_id': self.demo_firms[i]['id']}
            demo_projects.append(demo_project)
            curr_id += 1
        self.write_json(demo_projects, './data/projects.txt')
        return demo_projects

    def create_demo_documents(self):
        demo_documents = []
        curr_id = 1
        for demo_project in self.demo_projects:
            for i in range(10):
                demo_document = {'id': curr_id, 'name': 'Document {}'.format(curr_id),
                                 'file': 'document{}.pdf'.format(curr_id), 'project_id': demo_project['id'],
                                 'status': self.get_document_status(),
                                 'description': 'Document {} requested by {} to be received by {}'
                                     .format(i, demo_project['firm_id'], demo_project['client_id'])}
                demo_documents.append(demo_document)
                curr_id += 1
        self.write_json(demo_documents, './data/documents.txt')
        return demo_documents

    def get_document_status(self):
        status = {1: 'Requested', 2: 'Pending', 3: 'Approved'}
        return status[random.randint(1, 3)]

    def write_json(self, data, filename):
        with open(filename, 'w') as outfile:
            json.dump(data, outfile)
