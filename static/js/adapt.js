(function () {
    'use strict';

    angular
        .module('adapt', [
            'adapt.config',
            'adapt.routes',
            'adapt.authentication',
            'adapt.accountants',
            'adapt.clients',
            'adapt.utils',
            'angularModalService',
            'ngLoadScript',
        ]);

    angular
        .module('adapt.config', []);

    angular
        .module('adapt.routes', ['ngRoute']);

    angular
        .module('adapt')
        .run(run);


    run.$inject = ['$http'];

    function run($http) {
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
    }
})();