(function () {
    'use strict';

    angular
        .module('adapt.authentication.controllers')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['$location', 'Authentication'];

    function RegisterController($location, Authentication) {
        var vm = this;

        vm.register = register;

        function register() {
            Authentication.register(vm.first_name, vm.last_name, vm.email, vm.password);
        }
    }

    // angular
    //     .module('adpat')
    //     .controller('styleController', ['$scope', function ($scope) {
    //        $scope.coverStyle = 'cover.css';
    //        $scope.style = 'style.css';
    //     })
})();