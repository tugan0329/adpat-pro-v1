(function () {
    'use strict';

    angular
    .module('adapt.authentication.controllers')
    .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'Authentication'];

    function LoginController($location, Authentication) {
        var vm = this;

        vm.login = login;
        vm.error = false;

        function login () {
            Authentication.login(vm.email, vm.password);
        }

    }

    // angular
    //     .module('adpat')
    //     .controller('styleController', ['$scope', function ($scope) {
    //        $scope.coverStyle = 'cover.css';
    //        $scope.style = 'style.css';
    //     })
})();