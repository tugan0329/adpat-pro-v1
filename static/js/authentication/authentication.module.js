(function () {
    'use strict';

    angular
        .module('adapt.authentication', [
            'adapt.authentication.controllers',
            'adapt.authentication.services',
        ]);

    angular
        .module('adapt.authentication.controllers', []);

    angular
        .module('adapt.authentication.services', ['ngCookies']);

})();