(function () {
    'use strict';

    angular
        .module('adapt.authentication.services')
        .factory('Authentication', Authentication);

    Authentication.$inject = ['$cookies', '$http', 'Snackbar'];

    function Authentication($cookies, $http, Snackbar) {

        var Authentication = {
            getAuthenticatedAccount: getAuthenticatedAccount,
            isAuthenticated: isAuthenticated,
            login: login,
            logout: logout,
            register: register,
            setAuthenticatedAccount: setAuthenticatedAccount,
            unauthenticate: unauthenticate
        };

        return Authentication;

        ////////////////////

        function register(first_name, last_name, email, password) {
            return $http.post('/api/v1/accounts/', {
                first_name: first_name,
                last_name: last_name,
                email: email,
                password: password
                }).then(function(data, status, headers, config) {
                    Authentication.login(email, password);
                }, function(data, status, headers, config) {
                    console.log(data);
                    Snackbar.error(data.data.message);
            });
        }

        function login(email, password) {
            return $http.post('api/v1/auth/login/', {
                email: email, password: password
                }).then(function(data, status, headers, config) {
                    Authentication.setAuthenticatedAccount(data.data);
                    if(Authentication.getAuthenticatedAccount().is_client_hr) {
                        window.location = "/home";
                    } else {
                        window.location = "/home";
                    }
                }, function(data, status, headers, config) {
                    console.error('Epic failure!');
            });
        }

        function logout() {
            return $http.post('api/v1/auth/logout/')
                .then(function(data, status, headers, config) {
                    Authentication.unauthenticate();
                    window.location = '/login';
                }, function(data, status, headers, config) {
                    console.error('Epic failure!');
            });
        }


        function getAuthenticatedAccount() {
            if (!$cookies.get('authenticatedAccount')) {
                return;
            }
            return JSON.parse($cookies.get('authenticatedAccount'));
        }

        function isAuthenticated() {
            return !!$cookies.get('authenticatedAccount');
        }

        function setAuthenticatedAccount(account) {
            var now = new window.Date(),
                exp = new window.Date(now.getFullYear(), now.getMonth(), now.getDate()+7);

            $cookies.put('authenticatedAccount', JSON.stringify(account), {
                expires: exp
            });
        }

        function unauthenticate() {
            $cookies.remove('authenticatedAccount');
        }
    }
})();