(function () {
    'use strict';

    angular
        .module('adapt.utils', [
            'adapt.utils.services'
        ]);

    angular
        .module('adapt.utils.services', []);
})();