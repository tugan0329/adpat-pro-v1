(function () {
    'use strict';

    angular
        .module('adapt.accountants.services')
        .factory('Accountants', Accountants);

    Accountants.$inject = ['$http'];

    function Accountants($http) {
        var currClient;
        var currProject;
        var currDocument;

        var accountants = {
            getClients: getClients,
            getClient: getClient,
            getProjects: getProjects,
            getProjectsByClient: getProjectsByClient,
            getProject: getProject,
            getDocuments: getDocuments,
            getDocumentsByProject: getDocumentsByProject,
            getDocumentsByProject: getDocumentsByProject
        };

        return accountants;

        function getClients() {
            return $http.get('/data/clients.txt');
        }

        function getClient(id) {
            return $http.get('http://adapt-dev.us-west-2.elasticbeanstalk.com/api/v1/clients/');
        }

        function getProjects() {
            return $http.get('/data/projects.txt');
        }

        function getProjectsByClient(client) {
            return $http.get('http://adapt-dev.us-west-2.elasticbeanstalk.com/api/v1/projects/')
        }

        function getProject(id) {
            return $http.get('http://adapt-dev.us-west-2.elasticbeanstalk.com/api/v1/projects/');
        }

        function getDocuments() {
            return $http.get('/data/documents.txt');
        }

        function getDocumentsByProject(project) {
            return $http.get('http://adapt-dev.us-west-2.elasticbeanstalk.com/api/v1/documents/');
        }

        function getDocument(id) {
            return $http.get('http://adapt-dev.us-west-2.elasticbeanstalk.com/api/v1/documents/');
        }
    }
})();