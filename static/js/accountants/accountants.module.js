(function () {
    'use strict';

    angular
        .module('adapt.accountants', [
            'adapt.accountants.controllers',
            'adapt.accountants.services'
        ]);

    angular
        .module('adapt.accountants.controllers', []);

    angular
        .module('adapt.accountants.services', []);
})();