(function () {
    'use strict';

    angular
        .module('adapt.accountants.controllers')
        .controller('AccountantProjectListController', AccountantProjectListController);

    AccountantProjectListController.$inject = ['$location', 'Accountants', 'ModalService'];

    function AccountantProjectListController($location, Accountants, ModalService) {
        var vm = this;

        Accountants.getProjects().then(function(data, status, headers, config) {
            vm.projectList = data.data;

            vm.addProject = function() {
                ModalService.showModal({
                    templateUrl: "/static/templates/accountants/modals/addProject.html",
                    controller: "AddProjectController",
                    controllerAs: "vm"
                }).then(function(modal) {
                    modal.element.modal();
                });
            };

        }, function(data, status, headers, config) {
            console.log(status);
        });
    }
})();