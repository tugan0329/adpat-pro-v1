( function() {
    'use strict';

    angular
        .module('adapt.accountants.controllers')
        .controller('AddDocumentController', AddDocumentController);

    AddDocumentController.$inject = ['close', 'Accountants'];

    function AddDocumentController(close, Accountants) {
        var vm = this;

        Accountants.getProjects().then(function(data, status, headers, config) {
            vm.projectList = data.data;
            vm.project = vm.projectList[0];

            vm.close = function(result) {
                    close(result, 500);
                };

            vm.addDocument = function() {
                Accountants.addDocument(vm.name, vm.description, vm.file, vm.project);
                vm.close();
                };

        }, function(data, status, headers, config) {
            console.log(status);
        });
    }
})();