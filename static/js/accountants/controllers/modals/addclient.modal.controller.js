( function() {
    'use strict';

    angular
        .module('adapt.accountants.controllers')
        .controller('AddClientController', AddClientController);

    AddClientController.$inject = ['close', 'Accountants'];

    function AddClientController(close, Accountants) {
        var vm = this;

        vm.close = function(result) {
                close(result, 500);
            };

        vm.addClient = function() {
            Accountants.addClient(vm.name, vm.email, vm.address, vm.phone);
            vm.close();
            };
    }
})();