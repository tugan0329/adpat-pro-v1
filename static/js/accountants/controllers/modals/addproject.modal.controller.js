( function() {
    'use strict';

    angular
        .module('adapt.accountants.controllers')
        .controller('AddProjectController', AddProjectController);

    AddProjectController.$inject = ['close', 'Accountants'];

    function AddProjectController(close, Accountants) {
        var vm = this;

        Accountants.getClients().then(function(data, status, headers, config) {

            vm.clientList = data.data;
            vm.client = vm.clientList[0];

            vm.close = function(result) {
                    close(result, 500);
                };

            vm.addProject = function() {
                Accountants.addProject(vm.name, vm.client, vm.description, vm.startDate);
                vm.close();
                };

        }, function(data, status, headers, config) {
            console.log(status);
        });
    }
})();