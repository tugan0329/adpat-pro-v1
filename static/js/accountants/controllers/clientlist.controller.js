(function () {
    'use strict';

    angular
        .module('adapt.accountants.controllers')
        .controller('AccountantClientListController', AccountantClientListController);

    AccountantClientListController.$inject = ['$location', 'Accountants', 'ModalService'];

    function AccountantClientListController($location, Accountants, ModalService) {
        var vm = this;

        Accountants.getClients().then(function(data, status, headers, config) {
            vm.clientList = data.data;

            vm.addClient = function() {
                console.log('addClient triggered');
                ModalService.showModal({
                    templateUrl: "/static/templates/accountants/modals/addClient.html",
                    controller: "AddClientController",
                    controllerAs: "vm"
                }).then(function(modal) {
                    console.log('modal showed');
                    modal.element.modal();
                });
            };

        }, function(data, status, headers, config) {
            console.log(status);
        });
    }
})();