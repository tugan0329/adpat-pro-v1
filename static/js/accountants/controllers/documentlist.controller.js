(function () {
    'use strict';

    angular
    .module('adapt.accountants.controllers')
    .controller('AccountantDocumentListController', AccountantDocumentListController);

    AccountantDocumentListController.$inject = ['$location', 'Accountants', 'ModalService'];

    function AccountantDocumentListController($location, Accountants, ModalService) {
        var vm = this;

        Accountants.getDocuments().then(function(data, status, headers, config) {
            vm.documentList = _.filter(data.data, function(document) {
                    return document.project_id == 1;
                });

            vm.addDocument = function() {
                ModalService.showModal({
                    templateUrl: "/static/templates/accountants/modals/addDocument.html",
                    controller: "AddDocumentController",
                    controllerAs: "vm"
                }).then(function(modal) {
                    modal.element.modal();
                });
            };

        }, function(data, status, headers, config) {
            console.log(status);
        });
    }
})();