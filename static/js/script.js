$(document).ready(function(){

	// Sidebar Charts

	// Pie Chart
	var chart3 = c3.generate({
	   bindto: '#sidebar-piechart',
	    data: {
	         
	        // iris data from R
	        columns: [
	            ['1', 36],
	            ['2', 54],
	            ['3', 12],
	        ],
	        type : 'pie',
	        onclick: function (d, i) { console.log("onclick", d, i); },
	        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
	        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
	    },
	    color: {
	        pattern: ['#06c5ac','#3faae3','#ee634c','#6bbd95','#f4cc0b','#9b59b6','#16a085','#c0392b']
	    },
	    pie: {
	      expand: true
	    },
	    size: {
	      width: 140,
	      height: 140
	    },
	    tooltip: {
	      show: false
	    }

	});



	// Bar Chart
	var chart6 = c3.generate({
	    bindto: '#sidebar-barchart',
	    data: {
	        columns: [
	            ['data1', 30, 200, 100, 400, 250, 310, 90, 125, 50]
	        ],
	        type: 'bar'
	    },
	    bar: {
	        width: {
	            ratio: 0.8
	        }
	    },
	    size: {
	      width: 200,
	      height: 120
	    },
	    tooltip: {
	      show: false
	    },
	    color: {
	        pattern: ['#06c5ac','#3faae3','#ee634c','#6bbd95','#f4cc0b','#9b59b6','#16a085','#c0392b']
	    },
	    axis: {
	      y: {
	        show: false,
	        color: '#ffffff'
	      }
	}
	});


	// Sidebar Tabs
	$('#navTabs .sidebar-top-nav a').click(function (e) {
	 	e.preventDefault()
	 	$(this).tab('show');

	 	setTimeout(function(){
			$('.tab-content-scroller').perfectScrollbar('update');		 		
	 	}, 10);
	 	console.log('js triggered');

	});



	$('.subnav-toggle').click(function() {
		$(this).parent('.sidenav-dropdown').toggleClass('show-subnav');
		$(this).find('.fa-angle-down').toggleClass('fa-flip-vertical');
		console.log('js triggered');
	 	setTimeout(function(){
			$('.tab-content-scroller').perfectScrollbar('update');		 		
	 	}, 500);
	 	console.log('js triggered');

	});

   //  $('.sidenav-toggle').click(function() {
   //      $('#app-container').toggleClass('push-right');
	 	// setTimeout(function(){
			// $('.tab-content-scroller').perfectScrollbar('update');		 		
	 	// }, 500);
   //  });

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        if ($("#wrapper").hasClass("toggled")) {
        	$('#menu-toggle-tooltip').attr('data-original-title','open sidebar');
        }else{
        	$('#menu-toggle-tooltip').attr('data-original-title','close sidebar');
        }
        console.log('menu-toggle triggered');
    });


	$('.tab-content-scroller').perfectScrollbar();

	$('.theme-picker').click(function() {
		changeTheme($(this).attr('data-theme'));
	});


	function changeTheme(theme) {

		$('<link>')
		  .appendTo('head')
		  .attr({type : 'text/css', rel : 'stylesheet'})
		  .attr('href', '/css/app-'+theme+'.css');

		$.get('api/change-theme?theme='+theme);

	};

	// Activate Calendar

	$('#calendar2').fullCalendar({
	});




	// Line chart
	var lineChartData1 = {
        labels : ["","","","","","","","","","","",""],
        datasets : [
            {
                label: "My First dataset",
                fillColor : "rgba(6, 197, 172, 0.5)",
                strokeColor : "rgba(6, 197, 172, 1)",
                pointColor : "rgba(6, 197, 172, 1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(6, 197, 172, 1)",
                data : [65,59,80,81,56,55,40,74,36,65,33,55]
            },
            {
                label: "My Second dataset",
                fillColor : "rgba(244, 204, 11, 0.5)",
                strokeColor : "rgba(244, 204, 11, 1)",
                pointColor : "rgba(244, 204, 11, 1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(244, 204, 11, 1)",
                data : [28,48,40,29,86,27,60,45,27,59,68,38]
            }
        ]

    };


	// Set the date
	$('.today .date').text((new Date).getDate());
	
	var months = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	];

	$('.today .month').text(months[(new Date).getMonth()]);
	
	console.log('js is loaded');

	$(".sidebar-nav li:not(.sidenav-dropdown)").click(function() {
     	$("li.active").removeClass("active");
      	$(this).addClass('active');
	});



});