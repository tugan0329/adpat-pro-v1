(function () {
    'use strict';

    angular
        .module('adapt.routes')
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider.when('/', {
            controller: 'LoginController',
            controllerAs: 'vm',
            templateUrl: 'static/templates/authentication/login.html'
        }).when('/register', {
            controller: 'RegisterController',
            controllerAs: 'vm',
            templateUrl: 'static/templates/authentication/register.html'
        }).when('/login', {
            controller: 'LoginController',
            controllerAs: 'vm',
            templateUrl: 'static/templates/authentication/login.html'
        }).when('/accountant/clients', {
            controller: 'AccountantClientListController',
            controllerAs: 'vm',
            templateUrl: 'static/templates/accountants/clientList.html'
        }).when('/accountant/clients/projects', {
            controller: 'AccountantProjectListController',
            controllerAs: 'vm',
            templateUrl: 'static/templates/accountants/projectList.html'
        }).when('/accountant/clients/projects/documents', {
            controller: 'AccountantDocumentListController',
            controllerAs: 'vm',
            templateUrl: 'static/templates/accountants/documentList.html'
        }).when('/client/projects', {
            controller: 'ClientProjectListController',
            controllerAs: 'vm',
            templateUrl: 'static/templates/clients/projectList.html'
        }).when('/client/projects/documents', {
            controller: 'ClientDocumentListController',
            controllerAs: 'vm',
            templateUrl: 'static/templates/clients/documentList.html'
        }).when('/profile', {
            templateUrl: 'static/templates/profile.html'
        }).when('/home', {
            templateUrl: 'static/templates/home.html'
        }).when('/blank', {
            templateUrl: 'static/templates/blank.html'
        }).when('/compose', {
            templateUrl: 'static/templates/messages/compose.html'
        }).when('/inbox', {
            templateUrl: 'static/templates/messages/inbox.html'
        }).when('/calendar', {
            templateUrl: 'static/templates/calendar.html'
        });
    }
})();