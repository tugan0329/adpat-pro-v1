(function () {
    'use strict';

    angular
        .module('adapt.clients', [
            'adapt.clients.controllers',
            'adapt.clients.services'
        ]);

    angular
        .module('adapt.clients.controllers', []);

    angular
        .module('adapt.clients.services', []);
})();