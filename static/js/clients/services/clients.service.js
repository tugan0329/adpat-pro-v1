(function () {
    'use strict';

    angular
        .module('adapt.clients.services')
        .factory('Clients', Clients);

    Clients.$inject = ['$http'];

    function Clients($http) {

        var clients = {
            getProjects: getProjects,
            getProject: getProject,
            getDocuments: getDocuments,
            getDocumentsByProject: getDocumentsByProject
        };

        return clients;

        function getProjects() {
            return $http.get('/data/projects.txt');
        }

        function getProject(id) {
            return $http.get('/data/projects.txt');
        }

        function getDocuments() {
            return $http.get('/data/documents.txt');
        }

        function getDocumentsByProject(client) {
            return $http.get('/data/documents.txt');
        }
    }
})();