(function () {
    'use strict';

    angular
    .module('adapt.clients.controllers')
    .controller('ClientProjectListController', ClientProjectListController);

    ClientProjectListController.$inject = ['$location', 'Clients'];

    function ClientProjectListController($location, Clients) {
        var vm = this;

        Clients.getProjects().then(function(data, status, headers, config) {
            vm.projectList = data.data;
            
        }, function(data, status, headers, config) {
            console.log(status);
        });
    }
})();