(function () {
    'use strict';

    angular
    .module('adapt.clients.controllers')
    .controller('ClientDocumentListController', ClientDocumentListController);

    ClientDocumentListController.$inject = ['$location', 'Clients'];

    function ClientDocumentListController($location, Clients) {
        var vm = this;

        Clients.getDocuments().then(function(data, status, headers, config) {
            vm.documentList = data.data;

        }, function(data, status, headers, config) {
            console.log(status);
        });
    }
})();