After cloning the repo, run the following commands:

      $ npm install
      $ bower install

Then run:

     $ npm i simple-angular-server -g

To run the simple angular server, type:

     $ angularserver

The command line should say it is listening on port 3000. Open a browser and navigate to localhost:3000. You should see a login page for adapt.


To generate the demo json data, type the following commands:

    $ python

    $ >>> from createdemodata import DemoData
    $ >>> demodata = DemoData()
    $ >>> demodata.create_demo_data()

Django Login Credentials:
```
#!markdown

   admin url: http://adapt-dev.us-west-2.elasticbeanstalk.com/admin/
   api url: http://adapt-dev.us-west-2.elasticbeanstalk.com/api/v1/
   email: admin@adapt.com
   password: AdaptDev2016
```